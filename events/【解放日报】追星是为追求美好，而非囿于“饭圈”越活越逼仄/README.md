# 【解放日报】追星是为追求美好，而非囿于“饭圈”越活越逼仄
## 李愚

肖战粉丝引发的风波仍在持续。虽然肖战工作室已在微博上为粉丝行为造成的影响表示歉意，同时呼吁“所有的爱都是正面而积极向上的，希望大家可以理智追星”，但这个声明或许安抚了粉丝，却无法说服路人。

肖战粉丝可能从来都未曾料到：他们本意是支持偶像、拥护偶像的举动，会彻底走向反面，成为偶像出道以来最大的危机。凭借《陈情令》一夜蹿红的肖战，又一夜之间成为互联网上的“全民公敌”，令人唏嘘。因粉丝疯狂举动而形象大跌的偶像，肖战并非第一个。个别疯狂的粉丝热衷于通过辱骂、攻讦、人肉搜索等方式，攻击网络上针对自己偶像的不同意见和声音；涉及偶像的微博下面，常常涌动着粉丝们齐刷刷“控评”操作出来的同一种正面的声音……网友苦“饭圈”（粉丝圈子）久矣。此次风波如此之大，既是因为肖战粉丝以拥护偶像的名义逾越了边界；也是网友对“饭圈”不满情绪的凶猛宣泄。该引起反思的不仅是肖战粉丝，也是娱乐圈的“饭圈文化”。到底是什么时候、又是在哪里出了问题？

这几年，娱乐圈里的流量明星一茬接着一茬，演技乏善可陈，表演不甚用心，却顺着汩汩流量登至一部部大戏中的主角。今天，明星的生产机制已然发生变化。20年前走红的那批明星大多是科班毕业，在舞台上摸爬滚打多年，凭借优秀的作品、扎实的演技为观众所熟知。他们是明星，更是演员。互联网时代信息的分发渠道更为多元，年轻人的“根据地”已是互联网。曾经从优秀影视作品走出明星，而今演变成了从互联网走出明星，明星再转而进入影视圈。粉丝们通过他们在互联网上的话语权和影响力，直接或间接影响着资本对明星的选择：有时，只要有足够多的粉丝支持，无论演技如何这个人都可以成为片中“一号”。

这也是“流量明星”称呼的来源。他们是互联网流量制造出来的明星，是粉丝们用数据和金钱制造出来的明星。与此同时，明星与粉丝的关系也在发生变化。对于像陈道明、陈宝国、蒋雯丽这样的实力派演员来说，他们与粉丝的关系非常单纯，仅限于通过作品沟通，粉丝不去主动探究演员的私生活。但对于像肖战这样的流量明星来说，他们与粉丝的黏性极强。粉丝不仅仅是偶像的观望者，更是偶像的制作者、促成者、建构者，并从中获得一种主体感、掌控感、成就感。所以流量明星及其经纪公司，对粉丝可谓“爱三分、敬三分、怕三分”。水能载舟亦能覆舟，他们既需要与粉丝保持友好关系、保证流量的稳定，又畏惧于粉丝不理智的行为对于明星与经纪公司决策的干涉——此前就曾多次发生粉丝怒“撕”经纪人、经纪公司等事件，而明星也只能“食得咸鱼抵得渴”。

流量明星如果不依赖自身实力，仅靠仰仗粉丝爱的供养和金钱的供养，其实充满风险。不仅明星只能活成粉丝想要的“人设”，也在于“粉丝行为，偶像买单”。此次粉丝的出格举动，肖战注定需要一起承担。所以我们才一再呼吁，流量明星更该对职业心怀虔诚，用心提升基本的职业素养，唱歌的把歌唱好，演戏的把演技琢磨透了。当你成为一名真正的歌手和演员时，你才能断开粉丝的“乳汁”。

我们并不反对追星。诚如粉丝文化研究者杨玲所说，“说到底，明星和偶像只是一个中介物，他们让粉丝从尘世进入另外一个超越性的彼岸世界。”比如在这次疫情中，我们也看到许多在一线奋战的医生和护士，得到了来自他们偶像的鼓励。这是正面的追星，把偶像当成通往美好的中介物，经由“饭圈”抵达世界，让自己成为更好的人并热爱世界。

但不可否认，也存在着走向反面的追星行为。他们把饭圈视为“全世界”，因此隔绝了“饭圈”以外的声音和意见，哪怕是善意的批评都会被归为“异见”，非我族类、党同伐异。他们的世界越活越窄，格局也越来越逼仄。甚至，他们被制造明星的那份虚幻的“权力”所迷惑，企图将其运用于“饭圈”以外的世界，打压不同的声音、破坏他人的言说空间。殊不知，这种狭隘与越界有多疯狂，他们被反噬时就有多惨痛。此次风波，对流量明星与粉丝群体来说，都应该是重要的一课：是明星请学会用实力说话；是粉丝请记得跳出“饭圈”的四角天空，学会拥抱和热爱广阔的世界。


